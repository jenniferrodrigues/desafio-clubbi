from src.server.instance import server
from src.controllers.books import *
from flask import Flask
from flask_restful import Api
import logging
import config
import database
app = Flask(__name__)
api = Api(app)
app.config.from_object('config.BaseConfig')
database.init(app)


from src.resources.sellers import Sellers
from src.resources.products import Products
from src.resources.tokens import Tokens

logging.basicConfig(filename="ecommerce.log", filemode='a',
format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
datefmt='%H:%M:%S',level=logging.DEBUG)

logger = logging.getLogger(__name__)

logger.info("Registro das fontes.")


api.add_resource(Sellers, '/sellers')
api.add_resource(Products, '/products')
api.add_resource(Tokens, '/token')


if __name__ == '__main__':
    app.run(host='127.0.0.1', port=5000, debug=True, threaded=True)


# server.run()


