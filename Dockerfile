FROM python:3.7-alpine

RUN adduser -D challenge_ecommerce_Jennifer

WORKDIR /home/challenge_ecommerce_Jennifer

COPY requirements.txt requirements.txt
RUN python -m venv venv
RUN venv/bin/pip install -r requirements.txt

COPY app app
RUN chown -R challenge_ecommerce_Jennifer:challenge_ecommerce_Jenniferi ./
USER challenge_ecommerce_Jennifer

EXPOSE 5000
ENTRYPOINT ["venv/bin/python"]
CMD ["app/server.py"]