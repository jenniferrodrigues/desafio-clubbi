from flask import Flask
from flask import request
from flask_restplus import Api, Resource
from src.server.instance import server

app ,api= server.app, server.api
# faria a requisicao para o banco de dados
books_db =  [
    {'id':0,'title':'oi, pessoa'},
    {'id':1, 'title': 'coisa linda! pessoa do bem!'}
]

@api.route('/books')
class BookList(Resource):
    def get(self,):
        return books_db
    
    def post(self,):
        response = api.payload
        books_db.append(response)
        return response, 200