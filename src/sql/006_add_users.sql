use heroku_fedb3a90-0ef1-4f7d-9bf4-8651304311b8;
INSERT INTO role_types(id, role) VALUES(1, "SELLER");
INSERT INTO role_types(id, role) VALUES(2, "ADMIN");
INSERT INTO users (role_type_id, username, password_hash) values((SELECT id from role_types where role="SELLER"), "jennifer", "*F3A2A51A9B0F2BE2468926B4132313728C250DBF");
INSERT INTO sellers(user_id, name) values((select id from users where username="jennifer"), "jennifer");
INSERT INTO users (role_type_id, username, password_hash) values((SELECT id from role_types where role="ADMIN"), "admin", "*e3cbe60709e8abe2082c92cc5e72a762d5f18e22");
INSERT INTO sellers(user_id, name) values((select id from users where username="admin"), "admin");
