use heroku_fedb3a90-0ef1-4f7d-9bf4-8651304311b8;

create table users
(
    id                 INT             NOT NULL AUTO_INCREMENT,
    role_type_id       INT             NOT NULL,
    username           VARCHAR(255)    NOT NULL,
    password_hash      TEXT            NOT NULL,
    is_active                 BOOLEAN NOT NULL DEFAULT true,
    PRIMARY KEY(id),
    UNIQUE(username),
    FOREIGN KEY (role_type_id) REFERENCES role_types(id)

) ENGINE = INNODB DEFAULT CHARSET = utf8;

ALTER TABLE users ADD COLUMN creation_date DATETIME;
ALTER TABLE users ADD COLUMN last_update DATETIME;
ALTER TABLE users ADD COLUMN last_updated_by VARCHAR(150);
