from flask import jsonify, g
from flask_restful import Resource, reqparse

from models.models import auth

parser = reqparse.RequestParser()


class Tokens(Resource):
    

    @auth.login_required
    def get(self):
        
        token = g.user.generate_auth_token()
        return jsonify({'token': token.decode('ascii')})
