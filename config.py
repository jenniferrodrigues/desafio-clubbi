import os
import logging.config


class BaseConfig(object):
    DEBUG = False
    SQLALCHEMY_DATABASE_URI = os.getenv('CLEARDB_DATABASE_URL')
    SQLALCHEMY_DATABASE_URI = 'mysql://root@100senha/db_MeusLivros'
    SECRET_KEY = '100senha'


class TestConfig(BaseConfig):
    DEBUG = True
    TESTING = True
    WTF_CSRF_ENABLED = False
    SQLALCHEMY_DATABASE_URI = 'mysql://root@100senha/db_MeusLivros'
    

def configure_logging(logging_ini):
   
    logging.config.fileConfig(logging_ini)
    logging.info("Dados de loggin: '{0}'.".format(logging_ini))


def configure_logging_relative(logging_ini):
    

    base_dir = os.path.dirname(__file__)
    logging_ini = os.path.join(base_dir, logging_ini)
    configure_logging(logging_ini)


class DevelopmentConfig(BaseConfig):
    DEBUG = True


class ProductionConfig(BaseConfig):
    DEBUG = True
