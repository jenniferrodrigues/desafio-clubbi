# Desafio-clubbi

## Orientações
 Web API de CRUD em e-commerce
Sistema de gerenciamento de pedidos (orders). Após os clientes fazerem os pedidos, eles são
gerenciados internamente pela Clubbi.
Crie uma Web API, usando Python, node.js ou outra linguagem de escolha, que possibilita as
certas funções descritas abaixo.
A API deve ser exposta na web (rodar em localhost), e deve executar as funções frente à uma
base de dados mysql ou postgresql, que deve ser criada em um Docker container e seu
esquema e dados definidos no código a ser entregue.
Entidades obrigatórias de um pedido (order) e seus itens
Order: order_id, client_id, status
Item: order_id (se refere a uma order), descrição, product_id, unit_price (preço de 1 unidade do
produto)
Order
|__ item
|__ item
|__ ..

Funções da API a serem implementadas
- Criação de uma order
- Adicionar/remover um item de certa order, dado o order ID
- Retornar todos itens de uma order a partir de um ID
- Update do preço de itens, passado o product_id
- Cancelar uma order a partir de um order ID
Observações:
- Dica: comece criando uma modelagem de dados (schemas, tabelas). Use uma base de dados
real (mysql ou postgresql) localmente / docker.
- Gravar ao menos um exemplo de cada tipo de requisição, em curl, para ser entregue junto a
tarefa
- Enviar o material necessário para que possamos subir a base e adicionar dados a ela.
- Tratar os mais relevantes tipos de erros 4XX e 5XX
- Outras features da API que não foram descritas aqui podem ser implementadas e isso será
bem avaliado


```
## Pré requisitos:
Para executar esta aplicação você deve ter o Docker instalado.
* [Windows](https://docs.docker.com/windows/started)
* [OS X](https://docs.docker.com/mac/started/)
* [Linux](https://docs.docker.com/linux/started/)

## Uso:
Clone este repositório
```shell
$ git clone https://gitlab.com/jenniferrodrigues/desafio-clubbi
```
Navegue para o diretório clonado
```shell
$ cd desafio-clubbi
```
Monte o container
```shell
$ docker build desafio-clubbi .
```
Execute o container em segundo plano
```shell
$ docker run -d -p 5000:5000 desafio-clubbi
```

Acesse o endereço 127.0.0.1:5000/
```shell
Criar novo seller
$ curl http://ecommerce-rest-api.herokuapp.com/sellers -j -H "Content-Type: application/json" --data '{"username": "jennifer", "password":"100senha", "name": "jennifer"}'
{  "id": 44, "Sucesso": true }


```