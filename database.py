from flask import Flask
from flask_sqlalchemy import SQLAlchemy


import datetime
import time

db = None


def init(app):
    global db
    db = SQLAlchemy(app)

def clock():
    while True:
        print(datetime.datetime.now().strftime("%H:%M:%S"), end="\r")
        time.sleep(1)

clock()